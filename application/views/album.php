
  <table id="HUD" cellspacing="0">
        <tr>
            <td id="ericghud" class="hudsub"><button class="hudbut" onClick='window.location = "eric";'>Eric</button></td>
            <td id="chrisphud" class="hudsub"><button class="hudbut" onClick='window.location = "chris";'>Chris</button></td>
            <td id="chrishhud" class="hudsub"><button class="hudbut" onClick='window.location = "chrish";'>Chris H</button></td>
            <td id="justinmhud" class="hudsub"><button class="hudbut" onClick='window.location = "justinm";'>Justin</button></td>
            <td id="zachhud" class="hudsub"><button class="hudbut" onClick='window.location = "zac";'>Zac</button></td>
        </tr>
    </table>    
<div id="galleria">
            <?php
            foreach ($files as $file) {
            $path = $file['relative_path'];
            $name = $file['name'];
            echo '<img src="'.base_url().$path.'/'.$name.'">';
            };
            ?>
        </div>  
       
    <script>
    Galleria.loadTheme('<?php echo base_url()."JS/themes/classic/galleria.classic.js"; ?>');
    $('#galleria').galleria({
        thumb_crop: true, // crop all thumbnails to fit
        transition: 'fade', // crossfade photos
        transition_speed: 700, // slow down the crossfade
        data_config: function(img) {
            // will extract and return image captions from the source:
            return  {
                title: $(img).parent().next('strong').html(),
                description: $(img).parent().next('strong').next().html()
            };
        },
        extend: function() {
            this.bind(Galleria.IMAGE, function(e) {
                // bind a click event to the active image
                $(e.imageTarget).css('cursor','pointer').click(this.proxy(function() {
                    // open the image in a lightbox
                    this.openLightbox();
                }));
            });
        }
        });
    </script>