<div id="contact">
<img src="images/contact.jpg" id="contactpic" style="position:absolute; z-index:-1;display:none;" alt="Music City Studios">
<h1 Class="pagetitle">Contact Us</h1>
<div style="height:300px">
<p class="textfront">Thank you for visiting Music City Studios, a full-service portraiture company based in Nashville, Tennessee. We specialize in headshots and offer a wide variety of photographic services. Our company is a cooperative of five Vanderbilt University students with long-standing backgrounds in photography.</p>
<p class="textfront">Email: <a href="mailto:info@MusicCityStudios.com">info@MusicCityStudios.com</a></p>
<p class="textfront">Phone: (516) 708-4822</p>
<p class="textfront" ><button id="quote" style="margin-left:0;height:40px;margin-top:5px;">Get a Quote</button></p>
    <iframe style="margin-top:5px" src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FNashville-TN%2FMusic-City-Studios%2F154720488824&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=dark&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:35px;" allowTransparency="true"></iframe>
</div>
</div>

