<script type="text/javascript" src="<?php echo base_url()."JS/jq.js"; ?>"></script>
<script type="text/javascript" src="<?php echo base_url()."JS/jqui.js"; ?>"></script>
<?php if(isset ($currentpage)) { ?>
<script type="text/javascript" src="<?php echo base_url()."JS/galleria.js"; ?>"></script>
<?php };?>
<script type="text/javascript">
$(document).ready(function() {
var oldhtml = $('#notify').html();
var n = 0;
<?php if ($page == 'portfolio_view') { ?>
$('#photogs').fadeIn(500);
$('#ericg').addClass('portfoliosel');
$('.portfolio').mouseenter(function () {
$('.portfoliosel').html('').removeClass('portfoliosel');
$(this).addClass('portfoliosel');
$(this).html('<div id="clickport">Click to View Portfolio</div>');
if (n<6) {
$('#clickport').fadeIn(100).delay(500).fadeOut(200);
n += 1;
}
});
$('#ericg').click(function(){window.location = "<?php echo site_url(); ?>portfolio/eric";});
$('#chrish').click(function(){window.location = "<?php echo site_url(); ?>portfolio/chrish";});
$('#chrisp').click(function(){window.location = "<?php echo site_url(); ?>portfolio/chris";});
$('#justinm').click(function(){window.location = "<?php echo site_url(); ?>portfolio/justinm";});
$('#zach').click(function(){window.location = "<?php echo site_url(); ?>portfolio/zac";});
<?php } elseif ($page == 'home_view') {?>
if ($.browser.msie) {
   $('#promo1').show(500, function () {
     $('#promo2').show(500, function () {
         $('#20years').fadeIn(1000);
     });
   });	  
} else {
	   $('#promo1').fadeIn(500, function () {
    $('#promo2').fadeIn(500, function () {
        $('#20years').fadeIn(1000);
    });
  });	 
}
<?php } elseif ($page == 'contact_view') {?>
          $('#contactpic').fadeIn(500);
<?php } ?>
$('#home').click(function(){
  window.location = "<?php echo site_url(); ?>";});
$('#menportfolio').click(function(){
  window.location = "<?php echo site_url(); ?>portfolio";
});
$('#menpackages').click(function(){
  window.location = "<?php echo site_url(); ?>packages";});
$('#mencontact').click(function(){
  window.location = "<?php echo site_url(); ?>contact";});
<?php if(isset ($currentpage)) { ?>
$('<?php echo $currentpage; ?>').removeClass();
$('<?php echo $currentpage; ?>').addClass('current');
<?php };?>
$(".Menulink").hover( function () {
  $(this).addClass('Menulinkhover');
}).mouseleave( function () {$(this).switchClass('Menulinkhover','Menulink',400);
});
$('#quote').click( function () {
  $('#notifyback').show();
  $('#notify').show(100);
});
$('#close').click( function () {
   $('#notifyback').fadeOut(100);
     $('#notify').fadeOut(100);
});
   
   $('#quotesubmit').click( function () {
   var n= $('#name').val();
   var e= $('#email').val();
   var p= $('#phone').val();
   var d= $('#details').val();
   if (!n=="" && !e=="" && !p=="" && !d=="") {
   $.post("<?php echo site_url();?>email",$('#qform').serialize(), function () {
   $('#notifyback').fadeOut(100);
   $('#notify').fadeOut(100);
   $('#success').fadeIn(200).delay(300).fadeOut(200);
});
   } else {
       alert('Please fill in all feilds.');
       $('#name').focus();
   }        
   });   
});
</script>