<?php
$this->load->helper('url');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="google-site-verification" content="44Y4dWptljG6MQlYhX-69U474QsC8N5LwZ-DCmXBElc" />
<meta name="description" content="full-service portraiture company based in Nashville, Tennessee. We specialize in headshots and offer a wide variety of photographic services." />
<meta name="keywords" content="photography, headshots, studio, portraits, photographer" />
<meta name="author" content="Eric Glasser" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="/favicon.ico" />
<title><?php echo $title; ?></title>
<link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()."CSS/msc.css"; ?>"/>
<?php if(isset ($currentpage)) { ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()."CSS/portfolio.css"; ?>"/>
<?php };?>
<?php $this->load->view('jsfunc'); ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18770592-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="success">
    <h2 id="successtext">Success!</h2>
</div>
  <div id="notifyback">  </div>
        <div id="notify">
            <div id="quoteform">
            <h2 id="get">Get a Quote</h2>
            <form id="qform">
                <label for="name">Name</label><br/>
                <input type="text" id="name" name="name"/><br/>
                <label for="email">Email</label><br/>
                <input type="text" name="email" id="email"/><br/>
                <label for="phone">Phone</label><br/>
                <input type="text" name="phone" id="phone"/><br/>
                <label for="details">Description</label>
                <textarea id="details"name="details" rows="7" cols="50"></textarea><br/>
            </form>
            <button class="notifybutton"  id="close">Close</button><button id="quotesubmit" class="notifybutton">Submit</button>
            </div>
        </div>
    <table id="Main">
      <tr>
      <td id="header"><img id="home" src="<?php echo base_url(); ?>images/title.gif" /></td>
      <td class="MenuItem"><button class="Menulink" id="menportfolio">Photographers</button></td>
      <td class="MenuItem"><button class="Menulink" id="menpackages">Packages</button></td>
      <td class="MenuItem"><button class="Menulink" id="mencontact">Contact Us</button></td>
      </tr>
    </table>
