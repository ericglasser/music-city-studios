<div id="packages">
    <img src="images/pack.jpg" id="contactpic" style="position:absolute; z-index:-1" alt="Music City Studios">
        <button style="margin-top:55px;z-index:1;margin-left:650px;position:absolute;" id="quote" style="margin-left:0;"><p class="packdescription">Get a Quote</p></button>
    <h1 Class="pagetitle">Packages</h1>
    <table id="services" cellspacing="0">
        <tr>
            <td class="servicest"></td>
            <td class="services"><h2 class="packtitle">Portraits</h2></td>
            <td class="services"><h2 class="packtitle">Events</h2></td>
        </tr>
        <tr>
            <td class="servicest"><h2 class="packtitle">Price</h2></td>
            <td class="services"><p class="packdescription">Contact Us For Pricing</p></td>
            <td class="services"><p class="packdescription">Contact Us For Pricing</p></td>
        </tr>
      <tr>
            <td class="servicest"><h2 class="packtitle">Online Album</h2></td>
            <td class="services"><p class="packdescription">Included</p></td>
            <td class="services"><p class="packdescription">Included</p></td>
        </tr>
        <tr>
            <td class="servicest"><h2 class="packtitle">Minimum Shooting Time</h2></td>
            <td class="services"><p class="packdescription">30 Minutes</p></td>
            <td class="services"><p class="packdescription">1 Hour</p></td>
        </tr>
         <tr>
            <td class="servicest"><h2 class="packtitle">Basic Retouching</h2></td>
            <td class="services"><p class="packdescription">Included</p></td>
            <td class="services"><p class="packdescription">Included</p></td>
        </tr>
        <tr>
            <td class="servicest"><h2 class="packtitle">Comprehensive Retouching</h2></td>
            <td class="services"><p class="packdescription">$40 per image</p></td>
            <td class="services"><p class="packdescription">Ask for Quote</p></td>
        </tr>
         <tr>
            <td class="servicest"><h2 class="packtitle">Prints</h2></td>
            <td class="services" colspan="2"><p class="packdescription">Wallets,4x6,5x7,8x10, and larger. Printing starting at $10</p></td>
        </tr>
    </table>
</div>
