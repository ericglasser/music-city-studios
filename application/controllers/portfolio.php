<?php

class Portfolio extends Controller {

	function Portfolio()
	{
		parent::Controller();
	}

	function index() {
            $data['title']='Photographers';
            $data['page']='portfolio_view';
            $data['currentpage'] = '#menportfolio';
            $this->load->view('main_view',$data);
	}
        function eric() {
            $this->load->helper('file');
            $data['title']='Eric\'s Portfolio';
            $data['currentpage'] = '#menportfolio';
            $files=get_dir_file_info('images/portfolio/ericg');
            $data['files']=$files;
            $data['page']='album';
            $this->load->view('main_view',$data);
	}
          function zac()
	{
            $this->load->helper('file');
            $data['title']='Zac\'s Portfolio';
            $data['currentpage'] = '#menportfolio';
            $files=get_dir_file_info('images/portfolio/zach');
            $data['files']=$files;
            $data['page']='album';
            $this->load->view('main_view',$data);
	}
          function chris()
	{
            $this->load->helper('file');
            $data['title']='Chris\'s Portfolio';
            $data['currentpage'] = '#menportfolio';
            $files=get_dir_file_info('images/portfolio/chrisp');
            $data['files']=$files;
            $data['page']='album';
            $this->load->view('main_view',$data);
	}
          function chrish()
	{
            $this->load->helper('file');
            $data['title']='Chris H\'s Portfolio';
            $data['currentpage'] = '#menportfolio';
            $files=get_dir_file_info('images/portfolio/chrish');
            $data['files']=$files;
            $data['page']='album';
            $this->load->view('main_view',$data);
	}
          function justinm()
	{
            $this->load->helper('file');
            $data['title']='Justin\'s Portfolio';
            $data['currentpage'] = '#menportfolio';
            $files=get_dir_file_info('images/portfolio/justinm');
            $data['files']=$files;
            $data['page']='album';
            $this->load->view('main_view',$data);
	}
}

